/**
 * 
 */
package org.mixare;

import org.mixare.data.DataSource;
import org.mixare.gui.PaintScreen;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.util.Log;

/**
 * @author A.Egal
 * 
 */
public class ImageMarker extends Marker {

	public static final int MAX_OBJECTS = 15;
	private Bitmap image = null;

	public ImageMarker(String title, double latitude, double longitude,
			double altitude, String URL, Bitmap image, DataSource datasource) {
		super(title, latitude, longitude, altitude, URL, datasource);
		this.image = image;

	}
	
	public Bitmap getImage() {
		return this.image;
	}

	@Override
	public void update(Location curGPSFix) {
		super.update(curGPSFix);
	}

	@Override
	public int getMaxObjects() {
		return MAX_OBJECTS;
	}

	@Override
	public void draw(PaintScreen dw) {
		drawTextBlock(dw);

		if (isVisible) {

			float left = signMarker.x - image.getWidth() / 3;
			float top = signMarker.y - image.getHeight() / 3;

			dw.paintBitmap(image, left, top);

		}
	}
}