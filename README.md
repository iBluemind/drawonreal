DrawOnReal
==========

New augmented reality application for Android, based on Mixare(http://www.mixare.org).

오픈소스 증강현실 엔진인 Mixare(http://www.mixare.org) 을 기반으로 개발된 새로운 개념의
안드로이드용 증강현실 어플리케이션 DrawOnReal입니다.

DrawOnReal의 프로젝트 코드명은 qaz이며, 이는 DrawOnReal의 repository명이기도 합니다.
DrawOnReal은 안드로이드용 어플리케이션으로서 구글 플레이 스토어에서 배포되고 있습니다. (https://play.google.com/store/apps/details?id=com.qaz.client)

DrawOnReal은 GPLv3 라이센스 하에 배포된 Mixare를 기반으로 했기 때문에, DrawOnReal 역시
GPLv3 라이센스를 따르며, 이에 따라 본 repository를 공개하게 되었습니다.

DrawOnReal의 아이콘은 GPL 라이센스를 따르는 Sergio Sánchez López씨의 작품이며,
원본 URL은 http://www.kde-look.org/usermanager/search.php?username=Sephiroth6779 입니다.

DrawOnReal의 '그리기모드'(그림판) 소스코드는 http://android-town.org 에 공개된 'PaintBoard'를
이용한 것이며, 해당 URL은 http://147.46.109.56:9090/town/projects.jsp?sort=1&dir=%5Cbook05%5CPaintBoard 입니다.

DrawOnReal은 2012년 10월 26일에 처음 릴리즈되었으며, 이후 새로운 변경점 없이 
중요한 버그들을 픽스하는 작업만을 거쳐 2013년 7월 3일에 정식 버전 1.1.7을 마지막으로 릴리즈했습니다.
